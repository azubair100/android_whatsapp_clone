package com.parse.starter;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.util.ArrayList;
import java.util.List;

public class ChatActivity extends AppCompatActivity {

    String activeUser ="";

    //KEEP TRACK OF OLDER MESSAGES
    ArrayList<String> messages = new ArrayList<>();

    ArrayAdapter arrayAdapter;

    public void sendChat(View view) {

        final EditText chatEditText = (EditText) findViewById(R.id.chatEditText);

        ParseObject message = new ParseObject("Message");

        message.put("sender", ParseUser.getCurrentUser().getUsername());
        message.put("recipient", ParseUser.getCurrentUser().getUsername());
        message.put("message", chatEditText.getText().toString());

        String messageContent =  chatEditText.getText().toString();

        chatEditText.setText("");

        message.saveInBackground(
                new SaveCallback() {
                    @Override
                    public void done(ParseException e) {

                        if(e == null){

                            messages.add(chatEditText.getText().toString());
                            arrayAdapter.notifyDataSetChanged();

                            //Toast.makeText(ChatActivity.this, "Message sent", Toast.LENGTH_LONG).show();
                        }

                    }
                }
        );


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        Intent intent = getIntent();

        //DISPLAY ALL PREVIOUS MESSAGES
        String activeUser = intent.getStringExtra("username");

        ListView chatListView = (ListView) findViewById(R.id.chatListView);

        arrayAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, messages);

        chatListView.setAdapter(arrayAdapter);

        //DOUBLE  PARSE QUERY OR QUERY
        ParseQuery<ParseObject> query1 = new ParseQuery<ParseObject>("Message");
        query1.whereEqualTo("sender", ParseUser.getCurrentUser().getUsername());
        query1.whereEqualTo("recipient", activeUser);

        ParseQuery<ParseObject> query2 = new ParseQuery<ParseObject>("Message");
        query1.whereEqualTo("sender", activeUser);
        query1.whereEqualTo("recipient", ParseUser.getCurrentUser().getUsername());


        List<ParseQuery<ParseObject>> queries = new ArrayList<ParseQuery<ParseObject>>();

        queries.add(query1);
        queries.add(query2);


        ParseQuery<ParseObject> query = ParseQuery.or(queries);

        query.orderByDescending("createdAt");

        query.findInBackground(
                new FindCallback<ParseObject>() {
                    @Override
                    public void done(List<ParseObject> objects, ParseException e) {

                        if(e == null){

                            if(objects.size() > 0){

                                messages.clear();
                                for(ParseObject message : objects){

                                    String messageContent = message.getString("message");

                                    if(!message.getString("sender").equals(ParseUser.getCurrentUser().getUsername())){

                                        messageContent = " > "+ messageContent;

                                    }

                                    messages.add(messageContent);

                                }

                                arrayAdapter.notifyDataSetChanged();

                            }
                        }


                    }
                }
        );

    }
}
