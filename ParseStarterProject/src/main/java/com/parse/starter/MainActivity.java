/*
 * Copyright (c) 2015-present, Parse, LLC.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */
package com.parse.starter;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.LogInCallback;
import com.parse.Parse;
import com.parse.ParseAnalytics;
import com.parse.ParseAnonymousUtils;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.parse.SignUpCallback;


public class MainActivity extends AppCompatActivity {

  Boolean loginModeActive = false;

  public  void redirectIfLoggedin(){

    if(ParseUser.getCurrentUser() != null){

      Intent intent = new Intent(getApplicationContext(), UserListActivity.class);
      startActivity(intent);

    }

  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    redirectIfLoggedin();
    
    ParseAnalytics.trackAppOpenedInBackground(getIntent());
  }

  public void signupLogin(View view) {

    EditText username = (EditText) findViewById(R.id.userNameEditText) ;
    EditText password = (EditText) findViewById(R.id.paswordEditText) ;

    if(loginModeActive){

      ParseUser.logInInBackground(
              username.getText().toString(),
              password.getText().toString(),
              new LogInCallback() {
                @Override
                public void done(ParseUser user, ParseException e) {

                  if(e==null){

                    Log.i("s", "asdsa");

                    redirectIfLoggedin();
                  }

                  else{
                    //THE PROPER SOMETIMES DON'T GIVE PROPER ERROR MESSAGE

                    String message = e.getMessage();

                    if(message.toLowerCase().contains("java")){


                    }

                  }

                }
              }
      );



    }

    else{

      ParseUser user = new ParseUser();

      user.setUsername(username.getText().toString());
      user.setPassword(password.getText().toString());

      user.signUpInBackground(
              new SignUpCallback() {
                @Override
                public void done(ParseException e) {

                  if(e==null){

                    Log.i("OK", "Signeded up");

                    redirectIfLoggedin();


                  }

                  else{

                    Toast.makeText(MainActivity.this, e.getMessage().substring(e.getMessage().indexOf(" ")), Toast.LENGTH_LONG).show();

                  }

                }
              }
      );

    }



  }

  public void toggleLoginMode(View view) {

    Button signupOrLogin = (Button) findViewById(R.id.signupOrLogin);
    TextView toggleLoginModeTextView = (TextView) findViewById(R.id.toggleLoginModeTextView);

    if(loginModeActive){
      loginModeActive = false;
      signupOrLogin.setText("Sign Up");
      toggleLoginModeTextView.setText("Log In");

    }

    else{

      loginModeActive = true;
      signupOrLogin.setText("Log In");
      toggleLoginModeTextView.setText("Sign Up");

    }

  }
}